Squeezelite v1.9.x, Copyright 2012-2015 Adrian Smith, 2015-2023 Ralph Irving.

See the squeezelite manpage for usage details.
https://ralph-irving.github.io/squeezelite.html

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

Additional permission under GNU GPL version 3 section 7

If you modify this program, or any covered work, by linking or combining it with OpenSSL (or a modified version of that library), containing parts covered by the terms of The OpenSSL Project, the licensors of this program grant you additional permission to convey the resulting work. {Corresponding source for a non-source form of such a combination shall include the source code for the parts of OpenSSL used as well as that of the covered work.}

Contains dsd2pcm library Copyright 2009, 2011 Sebastian Gesemann which
is subject to its own license.

Contains the Daphile Project full dsd patch Copyright 2013-2017 Daphile,
which is subject to its own license.

Option to allow server side upsampling for PCM streams (-W) from
squeezelite-R2 (c) Marco Curti 2015, marcoc1712@gmail.com.

RaspberryPi minimal GPIO Interface
http://abyz.me.uk/rpi/pigpio/examples.html#Misc_minimal_gpio.

This software uses libraries from the FFmpeg project under
the LGPLv2.1 and its source can be downloaded from
https://sourceforge.net/projects/lmsclients/files/source/
